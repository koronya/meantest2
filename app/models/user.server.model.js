/**
 * Created by koronya on 2016. 10. 5..
 */

var mongoose = require('mongoose'),
	crypto = require('crypto'),
	Schema = mongoose.Schema;

var UserSchema = new Schema({
	username : {
		type : String,
		unique : true,
		index : true		// 보조 index
	},
	userid : {
		type : String,
		unique : true,		// primary key와 같음
		required : 'Username is required',	// 데이터가 없으면 저장하지 않음
		trim : true			// 자동으로 앞뒤 공백을 없애줌
	},

	password : {
		type : String,
		validate : [
			function(password) {
				return password && password.length >= 8;
			}, 'Password should be longer'
		]
	},

	salt : {
		type : String
	},

	provider : {
		type : String,
		required : 'Provider is required'
	},

	providerId : String,

	providerData : {},

	email : {
		type : String,
		match : [/.+\@.+\..+/, "pleas fill a valid e-mail address"]
	},


	//password : {
	//	type : String,
	//	validate : [
	//		function (password) {
	//			return password.length >= 8;		// 이 조건이 통과되는 조건
	//		},
	//		'Password should be longer'
	//	]
	//},
	//email : {
	//	type : String,
	//	//match : /.+\@.+@..+/	// email 패턴만 저장(정규표현식)
	//	match : /.+\@.+..+/	// email 패턴만 저장(정규표현식)
	//},


	role : {
		type : String,
		enum : ['Admin', 'Owner', 'User']	// 여기 열거된 data 외의 다른 것이 들어오면 저장안함
	},

	created : {
		type : Date,
		default : Date.now
	},

	// website는 daum.net으로 했을 때, db는 daum.net으로 저장되지만 읽어올 때, http://daum.net으로 읽어옴
	website : {
		type : String ,
		get : function(url) {
			if (!url) {
				return url;
			} else {
				if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0) {
					url = 'http://' + url;
				}

				return url;
			}
		}
	},

	// website3는 daum.net으로 했을 때, db에 http://daum.net으로 저장됨
	website3 : {
		type : String ,
		set : function(url) {
			if (!url) {
				return url;
			} else {
				if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0) {
					url = 'http://' + url;
				}

				return url;
			}
		}
	}
});


// pre는 해당 요청이 실행되기 전에 먼저 실행된다.
UserSchema.pre('save', function(next) {
	if (this.password) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'),
			'base64');
		this.password = this.hashPassword(this.password);
	}
	next();
});

UserSchema.methods.hashPassword = function(password) {
	return crypto.pbkdf2Sync(password, this.salt, 10000, 64).
	toString('base64');
};

UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

UserSchema.statics.findUniqueUserid = function(userid, suffix, callback) {
	var _this = this;
	var possibleUserid = userid + (suffix || '');

	_this.findOne({
		userid : possibleUserid
	}, function(err,user) {
		if (!err) {
			if (!user) {
				callback(possibleUserid);
			} else {
				return _this.findUniqueUserid(userid, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};



UserSchema.virtual('idpass').get(function() {
	return this.userid + ' ' + this.password;
});

// res.json() 을 사용하여 다큐먼트 데이터를 출력할 때
// get 옵션으로 정의한 값이 JSON에 포함되게 할 것
// 아래 코드를 적지 않으면 JSON으로 데이터를 표현할때 get 옵션을 무시하게 될 것
// 실제로 naver.com이 http://naver.com으로 되지 않고 그냥 나옴
//UserSchema.set('toJSON',{ getters : true});
UserSchema.set('toJSON',{ getters : true, virtuals : true});

mongoose.model('User', UserSchema);

