/**
 * Created by koronya on 2016. 10. 16..
 */

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ArticleSchema = new Schema({

	// 해당 글이 작성된 시간
	created : {
		type : Date,
		default : Date.now
	},

	// 해당 글의 제목
	title : {
		type : String,
		default : '',
		trim : true ,
		required : 'Title cannot be blank'
	},

	// 해당 글의 내용
	content : {
		type : String,
		default : '',
		trim : true
	},

	// 해당 글의 작성자 정보. User 모델을 참조하도록!
	creator : {
		type : Schema.ObjectId,
		ref : 'User'
	}
});

mongoose.model('Article', ArticleSchema);

