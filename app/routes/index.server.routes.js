/**
 * Created by koronya on 2016. 10. 3..
 */


module.exports = function(app) {
	var index = require('../controllers/index.server.controller');
	app.get('/', index.render);
}
