/**
 * Created by koronya on 2016. 10. 13..
 */

angular.module('example').controller('ExampleController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		$scope.authentication = Authentication;
		//$scope.name = Authentication.user ? Authentication.user.username : 'MEAN Application'
	}
]);
