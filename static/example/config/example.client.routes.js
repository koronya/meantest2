/**
 * Created by koronya on 2016. 10. 16..
 */

angular.module('example').config(['$routeProvider'
	,function($routeProvider) {
		$routeProvider.
			when('/', {
				templateUrl : 'example/views/example.client.view.html'
			}).
			otherwise({
				redirectTo : '/'
			});
	}
]);

