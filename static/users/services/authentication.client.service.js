/**
 * Created by koronya on 2016. 10. 16..
 */

angular.module('users').factory('Authentication', [
	function() {
		this.user = window.user;

		return {
			user : this.user
		}
	}
]);
