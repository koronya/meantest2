/**
 * Created by koronya on 2016. 10. 3..
 */

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
//process.env.NODE_ENV = process.env.NODE_ENV || 'production';

var express = require('./config/express_config'),
	mongoose = require('./config/mongoose'),
	passport = require('./config/passport');

var db = mongoose();
var app = express();
var passport = passport();

app.listen(3000);
module.exports = app;

console.log('Server running at localhost');


