/**
 * Created by koronya on 2016. 10. 4..
 */

var config = require('./config'),
	mongoose = require('mongoose');

module.exports = function() {

	var db = mongoose.connect(config.db);

	require('../app/models/user.server.model');
	require('../app/models/article.server.model.js');
	return db;
};

