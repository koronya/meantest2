/**
 * Created by koronya on 2016. 10. 3..
 */

var express = require('express'),
	morgan = require('morgan'),
	compress = require('compression'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override'),
	config = require('./config'),
	session = require('express-session'),
	passport = require('passport'),
	flash = require('connect-flash')
	;

module.exports = function() {
	var app = express();

	if(process.env.NODE_ENV === 'development') {
		app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
		app.use(compress());
	}

	app.use(bodyParser.urlencoded({
		extended : true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride());

	app.use(session({
		saveUninitialized : true,		// 초기화되지 않은 세션정보도 저장되는지에 관한 옵션
		resave : true,					// 같은 세션정보라도 다시 저장할건지에 대한 옵션
		secret : config.sessionSecret	// 비밀키를 설정
	}));

	app.set('views', './app/views');
	app.set('view engine', 'ejs');

	app.use(flash());
	app.use(passport.initialize());		// 패스포트 모듈을 초기화 시키며 구축
	app.use(passport.session());		// 사용자의 세션을 추적하기 위한 모듈

	require('../app/routes/index.server.routes')(app);
	require('../app/routes/users.server.routes')(app);
	require('../app/routes/articles.server.routes.js')(app);

	app.use(express.static('./static'));
	//app.use(express.static('static'));		// ./ 있어도 없어도 된다.
	//app.use(express.static('config/ss'));		이렇게 하면 config/ss가 static으로 지정됨. 여러개도 가능
	return app;
};

